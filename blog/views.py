from .models import Post
from django.views.generic import View, UpdateView, DeleteView
from .forms import PostForm, LoginForm, RegistrationForm
from django.utils import timezone

from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.http import HttpResponseRedirect

from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login
from django.db.models import Q

import datetime


# Подсчет количества статей на сайте
def post_home(request):

    post_count = Post.objects.filter(moder=True).count()
    time_now = datetime.datetime.now().strftime('%H:%M:%S')
    return render(request, 'blog/base.html', {"post_count": post_count, "time_now": time_now})


def about(request):
    return render(request, 'blog/about.html')


# Вводим поиск по заголовку или тексту на сайт
def articles(request):

    search_query = request.GET.get('search', '')

    if search_query:
        posts = Post.objects.filter(
            moder=True
        ).filter(
            Q(title__icontains=search_query) | Q(text__icontains=search_query)
        ).order_by(
            '-published_date'
        )
    else:
        posts = Post.objects.filter(moder=True).order_by('-published_date')
    # Постраничная пагинация
    paginator = Paginator(posts, 3)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request, 'blog/articles.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


# Создание новой статьи
def post_new(request):
    if request.method == "POST":
        # Используем request.FILES для редактирования и сохранения изображения
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


# Изменение статьи
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        # Используем request.FILES для редактирования и сохранения изображения
        form = PostForm(request.POST, request.FILES, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})


# Авторизация и вход существующего пользователя
class LoginView(View):

    def get(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        context = {'form': form}
        return render(request, 'blog/application.html', context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return HttpResponseRedirect('/')
        context = {'form': form}
        return render(request, 'blog/application.html', context)


# Регистрация нового пользователя
class RegistrationView(View):

    def get(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        context = {'form': form}
        return render(request, 'blog/registration.html', context)

    def post(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.username = form.cleaned_data['username']
            new_user.email = form.cleaned_data['email']
            new_user.save()
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()

            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            return HttpResponseRedirect('/')
        context = {'form': form}
        return render(request, 'blog/registration.html', context)


# Редактирование статьи
class PostUpdateView(UpdateView):
    model = Post
    template_name = 'blog/post_edit.html'

    form_class = PostForm


# Удаление статьи
class PostDeleteView(DeleteView):
    model = Post
    success_url = '/articles'
    template_name = 'blog/post_delete.html'
