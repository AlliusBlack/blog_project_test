from PIL import Image
from django.contrib import admin

from .models import Post
from django.forms import ModelForm, ValidationError
from django.utils.safestring import mark_safe


# Валидация картинки в админке
class ImageAdminForm(ModelForm):
    MIN_RESOLUTION = (400, 400)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe(
            '<span style="color:red; font-size: 12px;"> Загружайте изображение с минимальным расширением {}x{}</span>'
            .format(*self.MIN_RESOLUTION))

    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        min_width, min_height = self.MIN_RESOLUTION
        if img.width < min_width or img.height < min_height:
            raise ValidationError(' Разрешение изображение меньше минимального!')
        return image


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    form = ImageAdminForm
    list_display = ("id", "author", "title", "text", "created_date", "published_date", 'get_image')

    # Форма для отображения картинки в админке
    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="50" height"60"')

    get_image.short_description = 'Изображение'
    readonly_fields = ('get_image', )
