from django.db import models
from django.utils import timezone
from django.contrib.auth import settings


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    image = models.ImageField(verbose_name='Picture', null=True, upload_to='images/')
    moder = models.BooleanField('Модерация', default=False, help_text='Для публикации нужна проверка модератора')

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    # Возврат после редактирования
    def get_absolute_url(self):
        return f'/posts/{self.id}'

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
